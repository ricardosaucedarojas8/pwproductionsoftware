$(window).scroll(function() {
    if ($(this).scrollTop()>0)
     {
      $('.myDiv').show(250);
      
     }else if($(this).scrollTop()==0){
     $('.myDiv').hide(250);
     }
 });

// Disable form submissions if there are invalid fields
function sendForm(){ 
    // Get the forms we want to add validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {

        if (form.checkValidity() === false) {
          //$('#actionModal').modal('toggle');
          event.preventDefault();
          event.stopPropagation();

        }else{
          var nombre_completo = $("#nombre").val();
          var empresa = $("#empresa").val();
          var servicio = $("#servicio option:selected").text();
          var telefono = $("#telefono").val();
          var correo = $("#correo").val();

          //AQUI ME FALTA METER EL AJAX pa mandar los datos al Controller

        	//alert('-> ' + nombre_completo + ', ' + negocio + ', ' + servicio + ', ' +telefono + ', ' + correo );


          var parametros = {
              "nombre_completo" : nombre_completo,
              "empresa" : empresa,
              "servicio" : servicio,
              "telefono" : telefono,
              "correo" : correo,
              "function" : "addDataForm"
          };

          $.ajax({
              data:  parametros, //send data via AJAX
              url:   'controller/controllerForm.php', //url file controller PHP
              type:  'post', //send POST data
              beforeSend: function () {
                //document.getElementById("load").style.display = "block";
              },
              success:  function (response) { //get request
                
              if(response.success){ 
                $("#successModalTitle").html("<i class='fas fa-check-circle color-success'></i> ¡Éxito!");
                $("#successModalDescription").html(response.message);
                $("#contact").trigger("reset");
                $("#contact").removeClass("was-validated");
              }else{
                $("#successModalTitle").html("<i class='fas fa-exclamation-circle color-error'></i> ¡Error!");
                $("#successModalDescription").html(response.message);
              }
                
                $('#actionModal').modal('toggle');      
              }
          });

        }
        form.classList.add('was-validated');
    });

}