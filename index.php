<?php 

header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
header("Pragma: no-cache"); // HTTP 1.0.
header("Expires: 1800"); // Proxies.

?>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Diseño de Páginas Web, Precios Accesibles, Expertos en Posicionamiento Orgánico (SEO), Nuestros Servicios Incluyes Hosting, Dominio y Posicionamiento en Google, Campañas Publicitarias en Google Search (SEM) y Software a la Medida. Teléfono: 5548515096">
    <meta name="Keywords" content="DISEÑO WEB, DISEÑO DE PAGINAS WEB, ELABORACION DE PAGINAS WEB, DESARROLLO WEB, CREACION DE PAGINAS WEB, PAGINAS WEB ECONOMICAS, PAGINAS EN INTERNET, REGISTRO DE DOMINIOS, CORREO ELECTRONICO, SEO, POSICIONAMIENTO WEB, PAGINA DE INTERNET, PAGINAS PARA EMPRESAS, PROMOCION EN PAGINAS WEB, GOOGLE ADWORDS, CAMPAÑAS EN GOOGLE, PAGINAS WEB PROFESIONALES, CUANTO CUESTA UNA PAGINA WEB, PAGINAS WEB PRECIOS, DISEÑO DE PAGINAS WEB EN CDMX, DISEÑO DE PAGINAS WEB EN EL DF, PAGINAS WEB EN EL DF, PAGINAS WEB EN CDMX, CAMPAÑAS EN ADS, CAMPAÑAS EN EL BUSCADOR DE GOOGLE, CAMPAÑAS GOOGLE SEARCH">
    <meta name="title" content="Diseño de Paginas Web Premium en CDMX">
    <meta name="author" content="Páginas Web Premium">
    <meta name="Subject" content="Diseño de paginas Web">
    <meta name="Language" content="es">
    <meta name="Revisit-after" content="15 days">
    <meta name="Distribution" content="Global">
    <meta name="Robots" content="Index, follow">
    <meta name="theme-color" content="#000">
    <title>Diseño de Páginas Web en CDMX | Páginas Web Premium | Diseño de Tiendas Online en CDMX</title>

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <link href="carousel.css" rel="stylesheet" crossorigin="anonymous">

    <!-- Favicons -->
    <link rel="apple-touch-icon" href="assets/img/favicons/apple-touch-icon.png" sizes="180x180">

    <!-- Custom styles for this template -->
    <link href="album.css" rel="stylesheet">
  
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-125507839-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-125507839-1');
    </script>

    <!-- Google Analytics Events-->
    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-125507839-1', 'auto');
    ga('send', 'pageview');
    </script>
    <!-- End Google Analytics -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js" async></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" async></script>

  </head>
  <body>
  <header class="sticky-top position">
  <div class="bg-black collapse" id="navbarHeader" style="">
    <div class="container">
      <div class="row">
        <div class="col-sm-8 col-md-7 py-4">
          <h4 class="text-white">Páginas Web Premium</h4>
          <p class="text-white">En Páginas Web Premium creamos proyectos de diferentes tipos: Diseño de Páginas Web con código puro, Páginas Web con CMS autoadministrables, Diseño de Tiendas Online con codigo puro, Tiendas Online con CMS autoadministrables, Software a la Medida, Estrategias SEO y SEM en Google Ads, Google Search y Google My Business.</p>
        </div>
        <div class="col-sm-4 offset-md-1 py-4">
          <h4 class="text-white">Contacto</h4>
          <ul class="list-unstyled">
            <li><a href="http://facebook.com/paginaswebpremium" class="text-white" onclick="ga('send', 'event', 'inicio', 'visitarfacebook', 'clic')">Siguenos en Facebook</a></li>
            <li><a href="http://instagram.com/paginaswebpremium" class="text-white" onclick="ga('send', 'event', 'inicio', 'visitarinstagram', 'clic')">Siguenos en Instagram</a></li>
            <li><a href="https://wa.me/525567757255" class="text-white" onclick="ga('send', 'event', 'inicio', 'whats', 'clic')">Enviar Mensaje</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="navbar navbar-dark bg-black shadow-sm ">
    <div class="container d-flex justify-content-between">
      <a href="#" class="navbar-brand d-flex align-items-center">
        <img src="assets/img/paginas-web-premium-logo.png" alt="diseno-de-paginas-web">
      </a>
      <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
    </div>
  </div>
</header>

<main role="main">
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1" class=""></li>
      <li data-target="#myCarousel" data-slide-to="2" class=""></li>
      <li data-target="#myCarousel" data-slide-to="3" class=""></li>
      <li data-target="#myCarousel" data-slide-to="4" class=""></li>
      <li data-target="#myCarousel" data-slide-to="5" class=""></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active pwp-background-first pwp-background-first-mobile">
        <div class="container ">
          <div itemscope itemtype="http://schema.org/Dataset"  class="carousel-caption text-left">
            <h2 class="tittle-h2"><b itemprop="name">Diseño de Páginas Web</b></h2>
            <p class="text"><span itemprop="description">Exponer adecuadamente tu marca potencializa la oportunidad de que cada uno de los visitantes a tu página web se conviertan en clientes potenciales o próximos compradores. Recuerda que si tu negocio no está en internet, no existe. Cotiza tu Página Web, da clic en el siguiente botón.</span></p>
            <p><a class="btn btn-lg btn-primary btn-web-action" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'msjslide', 'clic')" >Enviar Mensaje</a>
            <a class="btn btn-lg btn-primary btn-mobile-action" href="tel:+525548515096" role="button" onclick="ga('send', 'event', 'inicio', 'callslide', 'clic')">Llamar Ahora</a>
          </p>
          </div>
        </div> 
      </div>
      <div class="carousel-item pwp-background-second pwp-background-second-mobile">
        
        <div class="container">
          <div class="carousel-caption">
            <h2 class="tittle-h2"><b>Tiendas Online</b></h2>
            <p class="text">Construimos tiendas online enfocadas en vender, creamos diseños agradables y ajustables, implementamos diferentes tipos de pagos y métodos de seguridad. Recuerda que para vender y enamorar, debes tener buena presentación.</p>
            <p><a class="btn btn-lg btn-primary btn-web-action" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'msjslide', 'clic')">Enviar Mensaje</a>
            <a class="btn btn-lg btn-primary btn-mobile-action" href="tel:+525548515096" onclick="ga('send', 'event', 'inicio', 'callslide', 'clic')">Llamar Ahora</a></p>
          </div>
        </div>
      </div>
      <div class="carousel-item pwp-background-third pwp-background-third-mobile">
        <div class="container">
          <div class="carousel-caption text-right">
            <h2 class="tittle-h2"><b>Estrategias SEO</b></h2>
            <p class="text">Preparamos tu página web con todo nuestro conocimiento y experiencia adquirida y te damos una serie de indicaciones que debes seguir para mantener tu página web en el top #5 en búsquedas orgánicas.</p>
            <p><a class="btn btn-lg btn-primary btn-web-action" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'msjslide', 'clic')">Enviar Mensaje</a>
            <a class="btn btn-lg btn-primary btn-mobile-action" href="tel:+525548515096" onclick="ga('send', 'event', 'inicio', 'callslide', 'clic')" >Llamar Ahora</a></p>
          </div>
        </div>
      </div>
      <div class="carousel-item pwp-background-four pwp-background-four-mobile">
        <div class="container">
          <div class="carousel-caption text-left">
            <h2 class="tittle-h2"><b>Actualización de Páginas Web</b></h2>
            <p class="text">Si ya tienes una página web, con hosting + dominio y deseas actualizarla, nosotros te podemos ayudar, todos nuestros diseños son adaptables a cualquier dispositivo y únicos como tu proyecto.</p>
            <p><a class="btn btn-lg btn-primary btn-web-action" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'msjslide', 'clic')">Enviar Mensaje</a>
            <a class="btn btn-lg btn-primary btn-mobile-action" href="tel:+525548515096" onclick="ga('send', 'event', 'inicio', 'callslide', 'clic')">Llamar Ahora</a></p>
          </div>
        </div>
      </div>
      <div class="carousel-item pwp-background-five pwp-background-five-mobile">
        <div class="container">
          <div class="carousel-caption">
            <h2 class="tittle-h2"><b>Campañas Publicitarias</b></h2>
            <p class="text">Construimos tiendas online enfocadas en vender, creamos diseños agradables y ajustables, implementamos diferentes tipos de pagos y métodos de seguridad. Recuerda que para vender y enamorar, debes tener buena presentación.</p>
            <p><a class="btn btn-lg btn-primary btn-web-action" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'msjslide', 'clic')">Enviar Mensaje</a>
            <a class="btn btn-lg btn-primary btn-mobile-action" href="tel:+525548515096" onclick="ga('send', 'event', 'inicio', 'callslide', 'clic')" >Llamar Ahora</a></p>
          </div>
        </div>
      </div>
      <div class="carousel-item pwp-background-six pwp-background-six-mobile">
        <div class="container">
          <div class="carousel-caption text-right">
            <h2 class="tittle-h2"><b>Logo Profesional</b></h2>
            <p class="text">Creamos logotipos con la más alta calidad, nos encargamos de plasmar en el logo todo lo que sientes y todos los valos que construyen a tu negocio o empresa. Nos encanta hacer propuestas innovadoras y crear proyectos con valor agregado.</p>
            <p><a class="btn btn-lg btn-primary btn-web-action" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'msjslide', 'clic')">Enviar Mensaje</a>
            <a class="btn btn-lg btn-primary btn-mobile-action" href="tel:+525548515096" onclick="ga('send', 'event', 'inicio', 'callslide', 'clic')">Llamar Ahora</a></p>
          </div>
        </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>

  <div class="container marketing">
    
  <div class="row featurette">
      <div class="col-md-7 order-md-2">
        <h2 class="featurette-heading-standard">Diseño de Páginas Web <span class="text-muted">a la Medida de tu Negocio.</span></h2>
        <p class="lead">Sabemos que todos los negocios son diferentes, es por ello que creamos diseños de páginas web únicos, actualmente contamos con experiencia en diferentes giros mercantiles; <b>Frutas y Verduras, Bandas Industriales, Inversiones Capitales, Accesorios para Mujeres, Venta de Autos hasta Servicios a Domicilio y más</b>, entregando resultados verdaderos y mejoras en el desempeño de su negocio.</p>
        <a class="btn btn-primary btn-lg btn-web-action shadow-lg" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'whats', 'clic')">Hablar con un Asesor</a>
        <a class="btn btn-primary btn-lg btn-mobile-action shadow-lg" href="tel:+525548515096" onclick="ga('send', 'event', 'inicio', 'llamar', 'clic')">Hablar con un Asesor</a>
      </div>
      <div class="col-md-5 order-md-1 ">
        <br>
        <img class="img-fluid shadow-lg" src="assets/img/paginas-web-en-cdmx.png" alt="diseno-de-paginas-web" >
        <br>
      </div>
  </div>
  <br><br><br>
  </div>


  <div class="container-fluid parallax"> 
    <div class="row">
      <div class="col-12" style="text-align: center;">
        <h2 class="featurette-heading-small horizontal-text-middle"><b>Páginas Web Premium</b></h2>
      </div>
    </div>
  </div>

  <div class="container marketing">
    <br><br><br>
    <div class="row">
       <div class="col-12" style="text-align: center;">
      <h2 class="featurette-heading-small">¿Porqué Nosotros?
      </h2>
      <p class="lead">Estamos seguros que todos nuestros proyectos de diferentes maneras crean mejoras en los negocios que han colaborado y siguen colaborando con nosotros. En Páginas Web Premium siempre le vamos a ofrecer:</p>
      <br><br>
    </div>

      <div class="col-lg-4 col-md-4 ">
        <img src="assets/img/paginas-web-alta-calidad.png" class="rounded" alt="diseno-de-paginas-web" width="140px" height="140px">
        <h2>Servicios de Calidad</h2>
        <p>Entregamos proyectos con diseños únicos. Confia en nosotros, te podemos ayudar.</p>
        
      </div><!-- /.col-lg-4 -->
      <div class="col-lg-4 col-md-4">
        <img src="assets/img/paginas-web-seguras-ssl.png" class="rounded" alt="diseno-de-paginas-web" width="140px" height="140px">
        <h2>Alta Seguridad</h2>
        <p>Nuestros Servicios Cuentan con candados COMODO SECURE™ – RapidSSL®.</p>
        
      </div><!-- /.col-lg-4 -->
      <div class="col-lg-4 col-md-4 ">
        <img src="assets/img/paginas-web-resultados.png" class="rounded" alt="diseno-de-paginas-web" width="140px" height="140px">
        <h2>Grandes Resultados</h2>
        <p>Estamos seguros que aumentarán tus ventas con nuestras estrategias digitales.</p>
        
    </div>
      <!-- /.col-lg-4 -->
    </div>
     <br><br>
    </div>
    <div id="particles-js"></div>
    <br><br><br>
    <div class="container marketing">
    <div class="row featurette">
      <div class="col-md-7">
        <h2 class="featurette-heading">Utilizamos estrategias efectivas. <span class="text-muted">Siempre dan buenos resultados.</span></h2>
        <p class="lead">Somos una agencia de software que ofrece desarrollo de proyectos web de alta calidad y a la medida, también ofrecemos consultoría en temas de posicionamiento para estrategias digitales en <b>Google Ads, Google Shopping, Google Analytics, Google Maps y Google My Business.</b></p>
        <a class="btn btn-success btn-lg btn-web-action shadow-lg" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'whats', 'clic')">Enviar Mensaje</a>
        <a class="btn btn-success btn-lg btn-mobile-action shadow-lg" href="tel:+525548515096" onclick="ga('send', 'event', 'inicio', 'llamar', 'clic')">Hablar con un Asesor</a>
        <br>
      </div>
      <div class="col-md-5" itemscope itemtype="http://schema.org/ImageObject" >
        
        <img class="img-fluid shadow-lg img-mobile-space" src="assets/img/estrategias-seo-y-sem-google-ads.png" alt="diseno-de-paginas-web" itemprop="contentUrl"> 
      </div>
    </div>
    <br>
    <br><br>
  </div>
  <div class="position-relative overflow-hidden p-3 p-md-5  text-center bg-grey outro" style="color:white; z-index:0;">
  <div class="col-md-5 p-lg-5 mx-auto my-5" style="z-index:9999 !important;">

    <h2 class="featurette-heading-small" style="text-shadow: 1px 1px 3px #000;"><b>Flexibilidad</b></h2>

    <p class="lead font-weight-normal" style="text-shadow: 1px 1px 3px #000;">Todos nuestrás paginas web estan disponibles en todos los navegadores web y todos los dispositivos móviles los 365 días del año.</p>
  </div>
  <div class="product-device shadow-sm d-none d-md-block shadow-lg"></div>
  <div class="product-device product-device-2 shadow-sm d-none d-md-block shadow-lg"></div>
</div>
  
  
  <div class="container marketing">
    <br><br><br>
  <div class="row featurette">
      <div class="col-md-7 order-md-2">
        <h2 class="featurette-heading-standard">Construimos estrategias SEO y SEM <span class="text-muted">con resultados asegurados.</span></h2>
        <p class="lead">Hasta ahora, hemos posicionado páginas web de diferentes giros mercantiles; desde <b>Frutas y Verduras, Bandas Industriales, Inversiones Capitales, Accesorios para Mujeres, Venta de Autos hasta Servicios a Docomilio y más</b>, entregado resultados verdaderos y mejoras en el desempeño de su negocio.</p>
        <a class="btn btn-primary btn-lg btn-web-action shadow-lg" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'whats', 'clic')">Hablar con un Asesor</a>
        <a class="btn btn-primary btn-lg btn-mobile-action shadow-lg" href="tel:+525548515096" onclick="ga('send', 'event', 'inicio', 'llamar', 'clic')">Hablar con un Asesor</a>
      </div>
      <div class="col-md-5 order-md-1 ">
        <img class="img-fluid shadow-lg img-mobile-space" src="assets/img/disenio-de-paginas-web-diseno-para-empresas.png" alt="diseno-de-paginas-web" >
      </div>
  </div>
  <br><br><br>
  </div>
  <section class="jumbotron text-center cta-bluemaster">
    <div class="container marketing">
      <h2 class="featurette-heading-standard"><b>¿Sabías que?</b></h2>
      <p class="lead">Con una PÁGINA WEB brindas CONFIANZA y SEGURIDAD a tus clientes, además puedes ofrecer información detallada de tus productos o servicios, horarios de atención, ubicación y contacto.  </p>
      <p>
        <a href="https://wa.me/525567757255" class="btn btn-success my-2 shadow-lg btn-lg" onclick="ga('send', 'event', 'inicio', 'whats', 'clic')">Enviar Whatsapp</a>
        <!--<a href="#" class="btn btn-primary my-2 shadow-lg btn-lg">Solicitar Cotización</a>-->
      </p>
    </div>
  </section>
  <!--
  <div class="row">
      <div class="col">
        <div class="spinner-grow text-primary" role="status">
  <span class="sr-only">Loading...</span>
</div>
      </div>
      <div class="col">
        <div class="spinner-grow text-secondary" role="status">
  <span class="sr-only">Loading...</span>
</div>
      </div>
      <div class="col">
        <div class="spinner-grow text-success" role="status">
  <span class="sr-only">Loading...</span>
</div>
      </div>
      <div class="col">
        <div class="spinner-grow text-danger" role="status">
  <span class="sr-only">Loading...</span>
</div>
      </div>
      <div class="col">
        <div class="spinner-grow text-warning" role="status">
  <span class="sr-only">Loading...</span>
</div>
      </div>
      <div class="col">
        <div class="spinner-grow text-info" role="status">
  <span class="sr-only">Loading...</span>
</div>
      </div>

      <div class="col">
        <div class="spinner-grow text-warning" role="status">
  <span class="sr-only">Loading...</span>
</div>
      </div>
      <div class="col">
        <div class="spinner-grow text-danger" role="status">
  <span class="sr-only">Loading...</span>
</div>
      </div>
      <div class="col">
        <div class="spinner-grow text-info" role="status">
  <span class="sr-only">Loading...</span>
</div>
      </div>
      <div class="col">
        <div class="spinner-grow text-dark" role="status">
  <span class="sr-only">Loading...</span>
</div>
      </div>
      <div class="col">
        <div class="spinner-grow text-success" role="status">
  <span class="sr-only">Loading...</span>
</div>
      </div>
      <div class="col">
        <div class="spinner-grow text-primary" role="status">
  <span class="sr-only">Loading...</span>
</div>
      </div>
  
      <div class="col">
        <div class="spinner-grow text-warning" role="status">
  <span class="sr-only">Loading...</span>
</div>
      </div>
      <div class="col">
        <div class="spinner-grow text-secondary" role="status">
  <span class="sr-only">Loading...</span>
</div>
      </div>
      <div class="col">
        <div class="spinner-grow text-success" role="status">
  <span class="sr-only">Loading...</span>
</div>
      </div> 
  </div>
  -->
  <div class="container marketing">
  <br><br>
  <div class="row">

    <div class="col-12" style="text-align: center;">
      <h2 class="featurette-heading-standard">Preguntas Frecuentes
      </h2>
      <p class="lead">A continuación listamos una pequeña serie de preguntas que hemos identificado como frecuentes o muy frecuentes, cualquier observación o comentario, no dudes y <b><a class="text-success" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'whats', 'clic')">envia un mensaje </a></b></p>
      <br><br>
    </div>


    <div class="col-md-4 col-sm-6">
      <div id="list-example" class="list-group">
        <a class="list-group-item list-group-item-action active disabled" href="#list-item-1">¿Que tipo de servicios ofrecemos?</a>
        <a class="list-group-item list-group-item-action disabled" href="#list-item-2">¿En que tiempo construyen mi página web?<a>
        <a class="list-group-item list-group-item-action disabled" href="#list-item-3">¿Mi proyecto es autoadministrable?</a>
        <a class="list-group-item list-group-item-action disabled" href="#list-item-4">¿Diseñan Tiendas en Linea?</a>
        <a class="list-group-item list-group-item-action disabled" href="#list-item-5">¿Qué metodos de pago aceptamos?</a>
      </div>
      <br>
    </div>
    <div class="col-md-8 col-sm-6 shadow-ask">
      <div data-spy="scroll" data-target="#list-example" data-offset="0" class="scrollspy-example">
        <h4 id="list-item-1">¿Que tipo de servicios ofrecemos?</h4>
        <p>En <b>Páginas Web Premium</b> contamos con más de 5 años de experiencia innovando en la industria 4.0, creando software a la medida y proyectos diversos como: <b>Páginas Web, Tiendas Online, Proyectos autoadministrables con CRM tipo Wordpress y Woocommerce, Sistemas Administradores de Procesos de Negocio a la Medida, Creación de Campañas Publicitarias en Google y Facebook, Diseños de Logotipos </b>y más...</p>
        <h4 id="list-item-2">¿En que tiempo construyen mi página web?</h4>
        <p><b>Tenemos el personal necesario y capacitado para hacer tu proyecto en menos de 8 días</b>, claro todo depende también la velocidad con la que nos mandes la información acerca de tu proyecto, pero sin duda alguna, en Páginas Web Premium <b>siempre entregamos los proyectos en un lapso de 7 a 14 días hábiles</b></p>
        <h4 id="list-item-3">¿Mi proyecto es Autoadministrable?</h4>
        <p><b>Si así lo prefieres tu, claro que si.</b> En Páginas Web Premium conocemos la Ingenieria del Software a nivel aplicación y lo manejamos de manera correcta, es por ello que podemos construir herramientas con código puro, tipo <b>(Java, PHP, AJAX, JSON, Jquery, Javascript, JSP's, Servlets, etc)</b> y proyectos con <b>herramientas CMS tipo Wordpress o Woocommerce</b>. Tenemos experiencia creando ambos tipos de proyectos, mucho siempre depende del cliente y de sus necesidades.</p>
        <h4 id="list-item-4">¿Diseñan Tiendas en Linea?</h4>
        <p>Claro, diseñamos tiendas online enfocadas en el mercado de tu empresa. <b>Tenemos más de 4 años trabajando en proyectos enfocados 100% en un proceso de venta cortos y efectivos.</b> Estos proyectos pueden ser con código puro o con herramientas <b>CMS tipo Wordpress + Woocommerce.</b></p>
        <h4 id="list-item-5">¿Que métodos de pago aceptamos?</h4>
        <p>Actualmente recibimos todas las tarjetas de <b>Credito y Débito</b>. Por mencionar algunas: <b>VISA, AMERICAN EXPRESS, MASTER CARD, MAESTRO y más...</b> La idea principal es tener la oportunidad de trabajar contigo y abrir el catálogo de oportunidades que tenemos disponibles para ti. También tenemos disponibles transferencias bancarias, transferencias interbancarias y depósitos directamente en caja.</p>
      </div>
    </div>
    <div class="col-12"> <br><br><br></div>
   
  </div>
  </div>
  
  <section class="jumbotron text-center cta-grey">
       <div class="container" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
          <h2 class="featurette-heading-small"><b>Solicitar Presupuesto</b> </h2>
          <p class="lead"><span itemprop="description">Estaremos encantados de escucharte, conocer tu proyecto, conectarnos contigo, responder a todas tus preguntas y hacer crecer tu negocio. Si estas interesado en crear tu página web, te ofrecemos un <span itemprop="availability">15% de Descuento soló por este mes.</span></span></p>
          <p><a class="btn btn-primary btn-lg btn-web-action" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'whats', 'clic')">Obtener Descuento</a>
    <a class="btn btn-primary btn-lg btn-mobile-action" href="tel:+525548515096" onclick="ga('send', 'event', 'inicio', 'llamar', 'clic')">Obtener Descuento</a></p>
      </div>
  </section>
  <div class="container marketing">
     <br><br><br>
  <div class="row text-center">
      <hr class="featurette-divider">
    <div class="col-12" style="text-align: center;">
      <h2 class="featurette-heading-small">Diseño de Páginas Web
      </h2>
      <p class="lead">Todos nuestros proyectos incluyen candado SSL, chat online, diseño ajustable, formulario de contacto, catálogo de productos o servicios, correo personalizado y acceso a redes sociales.</p>
      <br><br>
  </div>
  <div class="col-md-4">
          <div class="card mb-4 shadow-sm bg-primary color-text-white" >
        <div class="card-header">
          <h4 class="my-0 font-weight-normal"> <b>PLAN PREMIUM</b></h4>
        </div>
        <div class="card-body">
          <!--<h1 class="card-title pricing-card-title">$0 <small class="color-text-white">/ mo</small></h1>-->
            <ul class="mt-3 mb-4 text-left">
              <li><span><strong>10 secciones</strong></span></li>
              <li><span>Dominio <br>(<b>www.minegocio.com</b>).</span></li><li><span>Hosting <br>(<b>Alojamiento Web).</b></span></li>
              <li><span>Correos <br>(25 –&nbsp;<b>contacto@minegocio.com</b>).</span></li>
              <li><span>Productos o Servicios <br>(<b>50 a 150</b>).</span></li>
              <li><span>Actividades de SEO <br>(<b>Keywords – 30</b>).</span></li>
              <li><span>Certificado SSL <br>(<b>COMODO SECURE - https</b>).</span></li>
              <li><span>Compatible y Accesible <br>(<b>Android, iOS, Laptop, PC, Tabletas y Celulares</b>)</span></li>
              <li><span>Redes Sociales<b>&nbsp;<br>(FB, TW, IG)</b></span></li>
              <li><span>Chan en vivo, Chat Messenger &amp; Whatsapp</span></li>
            </ul>
            <a href="https://wa.me/525567757255" class="btn btn-lg btn-block btn-light btn-web-action" style="color:#28a745;" onclick="ga('send', 'event', 'inicio', 'whats', 'clic')">Contratar (-10% Desc)</a>
            <a href="tel:+525548515096" class="btn btn-lg btn-block btn-light btn-mobile-action" style="color:#28a745;" onclick="ga('send', 'event', 'inicio', 'llamar', 'clic')">Contratar (-10% Desc)</a>
          </div>
        </div>
      </div>
      <div class="col-md-4">

        <div class="card mb-4 shadow-sm bg-warning color-text-white">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal"> <b>PLAN EMPRESARIAL </b></h4>
          </div>
          <div class="card-body">
            <!--<h1 class="card-title pricing-card-title">$15 <small class="color-text-white">/ mo</small></h1>-->
            <ul class=" mt-3 mb-4 text-left">
              <li><span><strong>8 secciones</strong></span></li>
              <li><span>Dominio <br>(<b>www.minegocio.com</b>).</span></li><li><span>Hosting <br>(<b>Alojamiento Web</b>).</span></li>
              <li><span>Correos <br>(<b>10</b>).</span></li>
              <li><span>Productos o Servicios <br>(<b>25 a 50</b>).</span></li>
              <li><span>Actividades de SEO <br>(<b>Keywords – 20</b>).</span></li>
              <li><span>Certificado SSL <br>(<b>https</b>)</span></li>
              <li><span>Compatible y Accesible<br>(<b>Android, IOS, Laptop, PC, Tabletas y Celulares</b>).</span></li>
              <li><span>Redes Sociales<b> <br>(FB, TW ó IG).</b></span></li>
              <li><span>Chan en vivo, Whatsapp</span></li>
            </ul>
            <a href="https://wa.me/525567757255" class="btn btn-lg btn-block btn-success btn-web-action" onclick="ga('send', 'event', 'inicio', 'whats', 'clic')">Contratar Plan</a>

            <a href="tel:+525548515096"  class="btn btn-lg btn-block btn-success btn-mobile-action" onclick="ga('send', 'event', 'inicio', 'llamar', 'clic')">Llama Ahora</a>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="card mb-4 shadow-sm bg-success color-text-white">
      <div class="card-header">
        <h4 class="my-0 font-weight-normal"><b>PLAN BÁSICO</b></h4>
      </div>
      <div class="card-body">
        <!--<h1 class="card-title pricing-card-title">$29 <small class="color-text-white">/ mo</small></h1>-->
        <ul class=" mt-3 mb-4 text-left">
          <li><span><strong>8 secciones</strong></span></li>
          <li><span>Dominio <br>(<b>www.minegocio.com</b>).</span></li><li><span>Hosting <br>(<b>Alojamiento Web</b>).</span></li>
          <li><span>Correos <br>(<b>10</b>).</span></li>
          <li><span>Productos o Servicios <br>(<b>25 a 50</b>).</span></li>
          <li><span>Actividades de SEO <br>(<b>Keywords – 20</b>).</span></li>
          <li><span>Certificado SSL <br>(<b>https</b>).</span></li>
          <li><span>Compatible y Accesible <br>(<b>Android, IOS, Laptop, PC, Tabletas y Celulares</b>).</span></li>
          <li><span>Redes Sociales<br><b>(FB ó IG).</b></span></li>
        </ul>
        <a href="https://wa.me/525567757255" class="btn btn-lg btn-block btn-primary btn-web-action" onclick="ga('send', 'event', 'inicio', 'whats', 'clic')">Contratar Plan</a>
        <a href="tel:+525548515096"  class="btn btn-lg btn-block btn-primary btn-mobile-action" onclick="ga('send', 'event', 'inicio', 'llamada', 'clic')">Contratar Plan</a>
      </div>
    </div>
    </div>
  </div>
  <br><br><br>
</div>
  <section class="cta-bluemarine text-center padding-automatic-text">
    
    <h1>
      <a href="" class="typewrite text-color" data-period="2000" data-type='[ "Hola, somos Páginas Web Premium.", "Somos creativos y apasionados.", "Personas enfocadas y preparadas.", "Somos expertos, ofrecemos resultados.", "Te dejamos en buenas manos, ¿ok?" , "Si tienes duda, envia un mensaje, es muy sencillo." ]' >
        <span class="wrap"></span>
      </a>
    </h1>
    
  </section>

      <br><br>
  <div class="container ">
    <h2 class="featurette-heading">¿Estas interesado? <span class="text-muted">Solicita una Cotización.</span></h2>
    <p class="lead">Si estas interesad@ en crear una página web, una tienda online o alguna campaña publicitaria, deja tus datos y en menos de 20 minutos te atenderemos. Si deseas comunicación directa, es simple, <b><a class="text-success" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'inicio', 'whats', 'clic')">manda un whatsapp.</a></b></p>
    <br>
    <form id="contact" class="needs-validation" novalidate>
    <div class="form-group">
      <label for="nombre">Nombre del Interesado</label>
      <input type="text" class="form-control" id="nombre" placeholder="ej: Juan Manual Rodriguez" name="nombre" minlength="4" maxlength="40" required pattern="^[a-zA-Z0-9 ]*$">
      <div class="valid-feedback">Correcto.</div>
      <div class="invalid-feedback">Ingresa un nombre correcto. No aceptamos puros espacios en blanco. Min 5 Max 50 caracteres. No usar (*%/$+-.,).</div>
    </div>
    <div class="form-group">
      <label for="empresa">Nombre del Negocio ó Particular</label>
      <input type="text" class="form-control" id="empresa" placeholder="ej: MC Donals" name="empresa" minlength="5" maxlength="50" required pattern="^[a-zA-Z0-9 ]*$">
      <div class="valid-feedback">Correcto.</div>
      <div class="invalid-feedback">Ingresa un numero de empresa correcto. No aceptamos puros espacios en blanco. Min 5 Max 50 caracteres. No usar (*%/$+-.,).</div>
    </div>
    <div class="form-group">
      <label for="servicio">Servicio de Interés</label>
      <select class="form-control" id="servicio" name="servicio" required>
        <option value="">Selecciona una opción...</option>
        <option>Página Web</option>
        <option>Tienda Online</option>
        <option>Estrategias SEO</option>
        <option>Campañas Publicitarias</option>
      </select>
      
      <div class="valid-feedback">Correcto.</div>
      <div class="invalid-feedback">Selecciona una opción...</div>
    </div>
    <div class="form-group">
      <label for="telefono">Teléfono</label>
      <input class="form-control" type="tel" id="telefono" name="telefono" placeholder="ej: 5554344567" pattern="[0-9]{10}" required>
      <div class="valid-feedback">Correcto.</div>
      <div class="invalid-feedback">Ingresa un teléfono correcto, de 10 dígitos.</div>
    </div>
    <div class="form-group">
      <label for="correo">Correo</label>
      <input type="email" class="form-control" id="correo" placeholder="ej: contacto@paginaswebpremium.com.mx" name="correo" required>
      <div class="valid-feedback">Correcto.</div>
      <div class="invalid-feedback">Ingresa un correo correcto.</div>
    </div>
    <div class="form-group form-check">
      <label class="form-check-label">
        <input class="form-check-input" type="checkbox" name="terms" required> Acepto los <a href="">Términos y Condiciones</a>.
        <div class="valid-feedback">Correcto.</div>
        <div class="invalid-feedback">Es necesario que aceptes los términos y condiciones para continuar.</div>
      </label>
    </div>
    <a onclick="sendForm(this)" class="btn btn-success text-white">Solicitar Cotización</a>
  </form>
  <br><br><br>
  </div>

</main>

<div style="position: fixed;" id="" class="myDiv sticky">
  <p id="tienes_dudas">
    <strong class="text-success">¿Necesitas ayuda?</strong>
    <a class="call-cta-button" id="whats_link" href="https://wa.me/525567757255" onclick="ga('send', 'event', 'samsung', 'whats', 'clic')">
      <img src="assets/img/diseno-de-paginas-web-en-cdmx.png" alt="" class="wp-image-31 alignnone size-medium" width="30" height="30">
    </a>
    <a class="call-cta-button" id="llamar_link" href="tel:+525565020417" onclick="ga('send', 'event', 'samsung', 'llamada', 'clic')">
      <img src="assets/img/diseno-de-paginas-web.png" alt="" class="wp-image-31 alignnone size-medium " width="30" height="30" style="margin-right:35px;">
    </a>
  </p>
</div>

<!-- SUCCESS / FAIL ACTION MODAL -->
  
  <div class="modal fade" id="actionModal" tabindex="-1">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h2 class="modal-title"><p id="successModalTitle"></p></h2>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <form>
        <!-- Modal body -->
        <div class="modal-body">
          <p id="successModalDescription"></p>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button class="btn btn-success" type="button" data-dismiss="modal" onclick="this.form.reset();">Continuar</button>
        </div>
        </form>
      </div>
    </div>
  </div>

<footer class="text-muted bg-black" style="color: white !important; ">
  <div class="container">
    <p class="float-right">
      <a href="#" class="text-warning">Volver al Principio</a>
    </p>
    <p>Páginas Web Premium 2015 - 2020 | PROTECT DATA WITH COMODO SECURE&trade; – <span class="text-success">RapidSSL®</span> </p><p> <a href="https://blog.paginaswebpremium.com.mx" class="text-warning" onclick="ga('send', 'event', 'inicio', 'visitarblog', 'clic')">Estudia en nuestro blog</a> o si lo deseas, solicita una cotización <a href="https://wa.me/525567757255" class="text-info" onclick="ga('send', 'event', 'inicio', 'whats', 'clic')">dando click aquí.</a></p>
    <p>Horarios de Atención: 9:00 am a 7:00 pm de Lunes a Sábado.</p>
  </div>
</footer>

<script type="text/javascript" src="assets/js/letters.js"></script>
<script type="text/javascript" src="assets/js/form.js"></script>
<!--<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>-->
      <script>window.jQuery || document.write('<script src="/docs/4.4/assets/js/vendor/jquery.slim.min.js"><\/script>')</script>

      <script src="dist/js/bootstrap.bundle.min.js" integrity="sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm" crossorigin="anonymous"></script>

      <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

      <!-- scripts -->
  <script src="assets/js/particles.js"></script>
  <script src="assets/js/app.js"></script>


      

</body></html>
